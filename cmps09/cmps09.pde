#include <Wire.h>                     // Use I2C library
#define ADDRESS 0x60                  // Address of CMPS09

void setup(){
  Serial.begin(9600);                  
  Wire.begin();                       // Connect to I2C
}

void loop(){
   byte byteHigh, byteLow, fine;     // byteHigh/byteLow store high and low bytes of the bearing
                                     // fine stores decimal place of bearing
   char pitch, roll;                 // Stores pitch and roll (signed values)
   int bearing;                      // Full bearing
   
   Wire.beginTransmission(ADDRESS);  // begin communication with CMPS09
   Wire.send(2);                     // Start read
   Wire.endTransmission();
   Wire.requestFrom(ADDRESS, 4);     // Request 4 bytes from CMPS09
   while(Wire.available() < 4);      // Wait for the bytes to arrive
   byteHigh = Wire.receive();        // Store values
   byteLow = Wire.receive();            
   pitch = Wire.receive();              
   roll = Wire.receive();               
   
   bearing = ((byteHigh<<8) + byteLow) / 10;  // Calculate full bearing
   fine = ((byteHigh<<8) + byteLow) % 10;     // Calculate bearing decimal
   print_data(bearing, fine, pitch, roll);    // Print data
   
   delay(500);
}

void print_data(int bearing, int fine, int pitch, int roll) {
  // Print data to Serial Monitor window
  Serial.print("Bearing=");
  Serial.print(bearing, DEC);
  Serial.print(".");
  Serial.print(fine, DEC);

  Serial.print("\t");
  Serial.print("Pitch=");
  Serial.print(pitch, DEC);
  
  Serial.print("\t");
  Serial.print("Roll=");
  Serial.print(roll, DEC);
  Serial.println("");
}

