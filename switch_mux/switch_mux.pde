const int inputPin = 6;
const int strobePin = 5;
const int cPin = 2;
const int bPin = 3;
const int aPin = 4;

// Array of switch inputs
int store[] = {
  0,0,0,0,0,0,0,0};  

// A, B, C select input patterns
int cSelect[]={LOW,LOW,LOW,LOW,HIGH,HIGH,HIGH,HIGH};
int bSelect[]={LOW,LOW,HIGH,HIGH,LOW,LOW,HIGH,HIGH};
int aSelect[]={LOW,HIGH,LOW,HIGH,LOW,HIGH,LOW,HIGH};

void setup() {

  Serial.begin(9600);
  pinMode(inputPin, INPUT);    
  pinMode(strobePin, OUTPUT);
  pinMode(aPin, OUTPUT);  
  pinMode(bPin, OUTPUT);  
  pinMode(cPin, OUTPUT);
}

void loop() {

  for(int i=0; i<=7; i++) {
    // Send 1-of-8 Select patterns
    digitalWrite(aPin, aSelect[i]);
    digitalWrite(bPin, bSelect[i]);
    digitalWrite(cPin, cSelect[i]);

    // Set Strobe to read data
    digitalWrite(strobePin, LOW);

    // Read Output pin
    store[i] = digitalRead(inputPin);

    // Set Strobe back to HIGH
    digitalWrite(strobePin, HIGH);
  }

  // Display result as 8 bits
  for(int i=0; i<=7; i++)
    Serial.print(store[i]);
  Serial.println("");
  delay(250);
}

