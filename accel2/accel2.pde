const int xpin = A0;         // X-axis to analog pin 1
const int ypin = A1;         // Y-axis to analog pin 2
 
void setup()
{
  Serial.begin(9600);        // Set up for Serial Monitor
}

void loop()
{
  Serial.print(analogRead(xpin));     // Show X-axis value
  Serial.print("\t");

  Serial.print(analogRead(ypin));     // Show Y-axis  value
  Serial.println("");
  delay(100);                         // Delay between reads
}

