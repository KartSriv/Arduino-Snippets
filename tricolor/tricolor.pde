int redPin = 9;            // Red diode of LED
int greenPin = 10;         // Green diode of LED

void setup()  { 
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
}

void loop()  { 

  digitalWrite(redPin, HIGH);
  delay(500);
  digitalWrite(redPin, LOW);
  delay(500); 
  
  digitalWrite(greenPin, HIGH);
  delay(500);
  digitalWrite(greenPin, LOW);
  delay(500); 
  
  digitalWrite(redPin, HIGH);
  digitalWrite(greenPin, HIGH);  
  delay(500);
  digitalWrite(redPin, LOW);
  digitalWrite(greenPin, LOW);  
  delay(500); 
 
}

