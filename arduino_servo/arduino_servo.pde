:#include <Servo.h> 

Servo myServo;              // Create Servo object
int delayTime = 2000;       // Standard delay period (2 secs)
const int servoPin = 10;    // Use pin D10 for the servo

void setup() {              // Empty setup
} 

void loop() {               // Repeat these steps
  forward();                // Call forward, reverse, servoStop
  reverse();                //   user-defined functions
  servoStop();
  delay(3000);
} 

void forward() {            // Attach servo, go forward
  myServo.attach(servoPin); //   for delay period
  myServo.write(0);
  delay(delayTime);
  myServo.detach();         // Detach servo when done
}

void reverse() {            // Do same for other direction
  myServo.attach(servoPin);
  myServo.write(180);
  delay(delayTime);
  myServo.detach();
}

void servoStop() {          // Stop the servo by detaching
  myServo.detach(); 
}

