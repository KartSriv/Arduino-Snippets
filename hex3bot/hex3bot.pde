#include <Servo.h> 
 
Servo legsCenter;
Servo legsRight;
Servo legsLeft;
int delayLong = 250;
int delayShort = 350;
boolean debug = false;
 
void setup() 
{
  legsCenter.attach(12);
  legsRight.attach(11);
  legsLeft.attach(10);  
} 
 
void loop() 
{ 
  if(debug) {
    legs_neutral();
  } else {
  
    upL();
    delay(delayShort);
  
    legL_rev();
    legR_fwd();
    delay(delayLong);
    
    upR();
    delay(delayShort);  
    
    legL_fwd();
    legR_rev();
    delay(delayLong);
    
  }
} 

void upR() {          // Right side raised
  legsCenter.write(160);
}
void upL() {          // Left side raised
  legsCenter.write(20);
}

void legR_fwd() {
  legsRight.write(110);
}
void legR_rev() {
  legsRight.write(75);
}

void legL_fwd() {
  legsLeft.write(75);
}
void legL_rev() {
  legsLeft.write(110);
}

void legs_neutral() {
  legsCenter.write(90); 
  legsRight.write(90);
  legsLeft.write(90);  
}
