// Define how 74165 is connected to Arduino
const int dataPin = 11;    // Pin 9 of 74165
const int clockPin = 12;   // Pin 2 of 74165
const int latchPin = 13;   // Pin 1 of 74165

int tempStore = 0;
int dataStore = 0;

void setup() {
  Serial.begin(9600);
  pinMode(dataPin, INPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(latchPin, OUTPUT);
}

void loop() {
  shiftIn_165();            // Call shift function
  delay(250);               // Wait 1/4 second, then repeat
}

void shiftIn_165() {
  
  dataStore = 0;              // Store bits here

  digitalWrite(latchPin, 0);  // Load data on parallel lines
  delayMicroseconds(10);      // Short wait
  digitalWrite(latchPin, 1);  // Set to read

  for(int i=0; i<=7; i++){    // Cycle through all 8 bits
    digitalWrite(clockPin, 0);
    tempStore = digitalRead(dataPin); 
    if(tempStore)             // If bit is a 1
      dataStore = dataStore | (1 << i); // Shift bits into place
    digitalWrite(clockPin, 1);
  }
  binaryPad(dataStore, 8);    // Display in Serial Monitor

  //You may use the 8-bit value in the dataStore variable for
  // any additional processing. If dataStore>0 then you know
  // at least one switch is closed. Use getBit (or similar
  // programming statement) to determine which one(s), and act
  // upon it accordingly.
}

// Pad binary numbers with zeros, 
//  print result in Serial Monitor window
void binaryPad(int number, int bits) {
  int pad = 1;
  for (byte i=0; i<bits; i++) {
    if (number < pad)
      Serial.print("0");
    pad *= 2;
  }
  if (number == 0)
    Serial.println("");
  else
    Serial.println(number, BIN);    
}

