const int led =  13;        // Built-in LED pin
const int receiver = 12;    // Connect receiver to pin D12

void setup() {
  pinMode(led, OUTPUT);
  pinMode(receiver, INPUT);
}

void loop(){
  // read the receiver, LED on if detection
  if(digitalRead(receiver) == LOW) {
    digitalWrite(led, HIGH);
    delay(250);
  }
  digitalWrite(led, LOW);
}

