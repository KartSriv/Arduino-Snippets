'Flash led using gosub

high 1
low 2
gosub FlashLED
'... some other code here
stop

FlashLED:
  toggle 1
  toggle 2
  pause 100
  return
