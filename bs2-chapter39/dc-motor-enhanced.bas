'Operate DC motor - enhanced

Main:
  gosub motorOnFwd
  gosub waitLong
  gosub motorOff
  pause 1
  gosub motorOnRev
  gosub waitShort
  gosub motorOff
  goto Main

motorOnFwd:
  low 1
  high 0
  return

motorOnRev:
  high 1
  high 0
  return

motorOff:
  low 0
  return

waitLong:
  pause 5000
  return

waitShort:
  pause 1000
  return
