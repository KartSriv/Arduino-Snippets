'Operate DC motor

Main:
  low 1                     ' set direction to forward
  high 0                    ' turn on motor
  pause 2000                ' wait two seconds
  low 0                     ' turn off motor
  pause 100                 ' wait 1/10 second
  high 1                    ' set direction to reverse
  high 0                    ' turn on motor
  pause 2000                ' wait two seconds
  low 0                     ' turn off motor
  goto Main
