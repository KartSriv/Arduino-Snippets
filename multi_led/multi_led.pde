int latchPin = 8;     // Connected to Latch pin
int clockPin = 12;    // Connected to Clock pin
int dataPin = 11;     // Connected to Data pin

void setup() {
  // Set all pins to output
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
}

void loop() {
  // Count 0 to 255
  for (int val = 0; val <= 255; val++) {
    
    // Disable update during count
    digitalWrite(latchPin, LOW);
    // Shift out bits
    shiftOut(dataPin, clockPin, MSBFIRST, val);

    // Activate LEDs
    digitalWrite(latchPin, HIGH);    

    // Short delay to next update
    delay(10);
  }

}

