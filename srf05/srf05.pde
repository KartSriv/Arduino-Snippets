int duration;                 // Stores duration of pulse
int distance;                 // Stores distance
int srfPin = 2;               // SRF05 connected to digital pin D2

void setup() {
  Serial.begin(9600);
}

void loop() {
  pinMode(srfPin, OUTPUT);	    // Set pin to OUTPUT
  digitalWrite(srfPin, LOW);	    // Ensure pin is low
  delayMicroseconds(2);
  digitalWrite(srfPin, HIGH);       // Start ranging 
  delayMicroseconds(10);            //  with 10 microsecond burst
  digitalWrite(srfPin, LOW);        // End ranging
  pinMode(srfPin, INPUT);	    // Set pin to INPUT
  duration = pulseIn(srfPin, HIGH); // Read echo pulse
  distance = duration / 74 / 2;     // Convert to inches
  Serial.println(distance);         // Show distance in Serial Monitor
  delay(100);
}

