int motDirection = 4;                // Direction line to pin D4
int motEnable = 5;                   // Enable/PWM to pin D5
 
void setup() {
  // Pins 11 and 12 as outputs
  pinMode(motDirection, OUTPUT);
  pinMode(motEnable, OUTPUT);
}

void loop() {
  digitalWrite(motDirection, LOW);   // Set direction
  digitalWrite(motEnable, HIGH);     // Turn motor on
  delay(2000);                       // Wait 2 seconds
  digitalWrite(motDirection, HIGH);  // Reverse direction
  delay(2000);                       // Wait 2 seconds
  digitalWrite(motEnable, LOW);      // Turn motor off
  digitalWrite(motDirection, LOW);   // Set direction
  analogWrite(motEnable, 128);       // Set motor to half speed
  delay(2000);                       // Wait 2 seconds
}

