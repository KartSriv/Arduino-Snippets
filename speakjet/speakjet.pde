// Use SoftwareSerial library (comes with Arduino IDE)
#include <SoftwareSerial.h>

#define txSerial 2  // Serial out
#define rxSerial 3  // Serial in (not used for SpeakJet, but
                    //  must be defined anyway
SoftwareSerial SpeakJet =  SoftwareSerial(rxSerial, txSerial);

void setup()
{
  pinMode(txSerial, OUTPUT);
  SpeakJet.begin(9600);  // Standard SpeakJet baud rate
}

void loop()
{
  // Set up speech phrase ("I am a robot")
  char phrase[] = {20, 96, 21, 114, 22, 88, 23, 5, 157, 132, 132, 140, 154, 128, 148, 7, 137, 7, 164, 18, 171, 136, 191};
  SpeakJet.println(phrase);  // Send phrase to SpeakJet
  delay (4000);              // Wait 4 seconds; repeat
}
