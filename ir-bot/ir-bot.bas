main:
	infrain2
  
  gosub l_stop
  gosub r_stop

  select case infra


    case 0		'1 - left motor forward
      gosub l_fwd
    
    case 1		'2 - left+right forward
    	gosub l_fwd
      gosub r_fwd
      
    case 2		'3 - right motor forward
    	gosub r_fwd
      
    case 3		'4 - left motor stop
    	gosub l_stop
    
    case 4		'5 - all stop
      gosub l_stop
      gosub r_stop
    
    case 5		'6 - right motor stop
			gosub r_stop
			    
    case 6		'7 - left motor reverse
			gosub l_rev
    
    case 7		'8 - left+right reverse
    	gosub l_rev
    	gosub r_rev
    
    case 8		'9 - right motor reverse
    	gosub r_rev
    
  endselect
  
  goto main
  
'---
'Motor control routines

l_fwd:
	LOW 0
	HIGH 1
	return

r_fwd:
	LOW 2
	HIGH 3
	return
	
l_stop:
	LOW 0
	LOW 1
	return

r_stop:
	LOW 2
	LOW 3
	return
	
l_rev:
	LOW 1
	HIGH 0
	return

r_rev:
	HIGH 2
	LOW 3
	return
