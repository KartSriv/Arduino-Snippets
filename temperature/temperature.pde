int lm34 = A0;
int tempF = 0;

void setup() {
  Serial.begin(9600);
}

void loop() {
  tempF = (500.0 * analogRead(lm34)) / 1024;
  Serial.print("Current temperature: ");
  Serial.println(tempF, DEC);
  delay(500);
}

