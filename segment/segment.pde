int outA = 8;     // Connected to A pin on 4511
int outB = 9;    // Connected to B pin
int outC = 10;    // Connected to C pin
int outD = 11;    // Connected to D pin

void setup() {
  // Set all pins to output
  pinMode(outA, OUTPUT);
  pinMode(outB, OUTPUT);
  pinMode(outC, OUTPUT);
  pinMode(outD, OUTPUT);
}

void loop() {
  // Display 3
  digitalWrite(outA, HIGH);
  digitalWrite(outB, HIGH);
  digitalWrite(outC, LOW);
  digitalWrite(outD, LOW);
  delay (1000);

  // Display 9
  digitalWrite(outA, HIGH);
  digitalWrite(outB, LOW);
  digitalWrite(outC, LOW);
  digitalWrite(outD, HIGH);
  delay (1000);
}
