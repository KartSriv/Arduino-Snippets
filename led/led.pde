void setup() { 
  pinMode(12, OUTPUT);      // Make D13 an output
}

void loop() {
  digitalWrite(12, HIGH);   // Turn LED on
  delay(500);               // Wait 1/2 second (500 ms)
  digitalWrite(12, LOW);    // Turn LED off
  delay(500); 
}

