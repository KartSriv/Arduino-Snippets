int distance = 0;
int averaging = 0;

void setup() {
  Serial.begin(9600);    // Use Serial Monitor window
}

void loop() {
  // Get a sampling of 10 readings from sensor
  for (int i=0; i <= 10; i++) {
    distance = analogRead(A0);
    averaging = averaging + distance;
    delay(1);
  }
  // Average out the 10 readings
  distance = averaging / 10;
  averaging = 0;  
  Serial.println(distance, DEC);  // Display result
  delay(250);                     // Short delay before next
}

