const int eye = A2;
const int LED = 8;

void setup() { 
  Serial.begin(9600);
  pinMode(LED, OUTPUT);
  digitalWrite(LED, HIGH);
} 
 
void loop() { 
  Serial.println(analogRead(eye), DEC);
  delay(500);
}
