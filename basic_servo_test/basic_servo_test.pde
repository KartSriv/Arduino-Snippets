#include <Servo.h>    // Use the Servo library, included with
                      //   the Arduino IDE (version 0017 or later)

Servo myServo;        // Create Servo object to control the servo 
int delayTime = 2000; // Delay period, in milliseconds

void setup() { 
  myServo.attach(9);  // Servo is connected to pin D9 
} 

void loop() { 
  myServo.write(0);   // Rotate servo to position 0
  delay(delayTime);   // Wait delay
  myServo.write(180); // Rotate servo to position 180
  delay(delayTime);   // Wait again
}