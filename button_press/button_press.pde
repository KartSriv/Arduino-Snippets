const int led = 13;   // Built-in LED
int bumperA = 12;     // Digital pin D12
int bumperB = 0;      // Interript 0 (digital pin D2)

void setup()
{
  pinMode(led, OUTPUT);
  digitalWrite(led, LOW);  
  // Set up interrupt to trigger on rising edge
  //  of switch transition
  attachInterrupt(bumperB, handle_interrupt, RISING);
}

void loop() {
  // Main loop
  poll();
}

void poll() {
  //Continually set state of LED 
  // based on bumperA switch
  digitalWrite(led, digitalRead(bumperA));
}

void handle_interrupt() {
  // Interrupt handler for bumperB switch
  // Flash LED during interrupt to show the 
  //  interrupt was handled
  digitalWrite(led, HIGH);
  for (int i=0; i <= 1000; i++)
    delayMicroseconds(1000);  
  digitalWrite(led, LOW);
  for (int i=0; i <= 1000; i++)
    delayMicroseconds(1000);  
}

