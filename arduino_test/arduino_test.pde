const int LED = 13;          // LED to digital pin 13 (D13)
const int POT = A0;          // pot to analog pin 0 (A0)
int potValue = 0;            // variable for pot value

void setup() {
  // initialize digital pin 13 as an output
  pinMode(LED, OUTPUT);     
}

void loop() {
  potValue = analogRead(POT);  // read pot value 
  digitalWrite(LED, HIGH);     // turn LED on
  delay(potValue);             // wait for value (milliseconds)
  digitalWrite(LED, LOW);      // turn LED off
  delay(potValue);             // wait for value (milliseconds)
}

