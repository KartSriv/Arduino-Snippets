int redPin = 9;            // Red diode of LED
int greenPin = 10;         // Green diode of LED
int bluePin = 11;          // Blue diode of LED

void setup()  { 
 
  // Start with everything off
  analogWrite(redPin, 0);
  analogWrite(greenPin, 0);
  analogWrite(bluePin, 0);
} 

void loop()  { 

  // Fade red in and out
  for(int fadeValue = 0 ; fadeValue <= 255; fadeValue++) { 
    analogWrite(redPin, fadeValue);
    delay(5);
  }
  for(int fadeValue = 255 ; fadeValue >= 0; fadeValue--) { 
    analogWrite(redPin, fadeValue);
    delay(5);
  }
  
  // Fade in blue, then add green, and finally red
  for(int fadeValue = 0 ; fadeValue <= 255; fadeValue++) { 
    analogWrite(bluePin, fadeValue);
    delay(5);
  }
  for(int fadeValue = 0 ; fadeValue <= 255; fadeValue++) { 
    analogWrite(greenPin, fadeValue);
    delay(5);
  }
  for(int fadeValue = 0 ; fadeValue <= 255; fadeValue++) { 
    analogWrite(redPin, fadeValue);
    delay(5);
  }
  
  // Wait 2 seconds before doing it all again
  // Note that all three colors may not create a 
  //   pure "white" because the diodes in the LED
  //   do not overlap one another
  delay(2000);
  
  analogWrite(redPin, 0);
  analogWrite(greenPin, 0);
  analogWrite(bluePin, 0);
  delay(500);
 
}

