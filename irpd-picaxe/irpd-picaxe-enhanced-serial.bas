symbol IR_L = 1         ' Left
symbol IR_R = 4         ' Right
pwmout 2, 25, 52        ' Approx. 38.4 kHz

Main:
  gosub ToggleL
  gosub ToggleR
  goto Main

ToggleL:					' Toggle right on/off
  low IR_L
  pause 50
  if pin3=0 then 
  	sertxd("Something to the left", 13, 10)
  end if
  high IR_L
  pause 20  
  return

ToggleR:           ' Toggle right on/off
  low IR_R
  pause 50
  if pin3=0 then 
  	sertxd("Something to the right", 13, 10)  
  end if
  high IR_R
  pause 20
  return

