symbol IR_L = 1         ' Left
symbol IR_R = 4         ' Right
symbol LED = 0					' Results LED
pwmout 2, 25, 52        ' Approx. 38.4 kHz

Main:
  gosub ToggleL
  gosub ToggleR
  goto Main

ToggleL:					' Toggle right on/off
  low IR_L
  pause 50
  if pin3=0 then gosub f_flash
  high IR_L
  pause 20  
  return

ToggleR:           ' Toggle right on/off
  low IR_R
  pause 50
  if pin3=0 then gosub s_flash  
  high IR_R
  pause 20
  return

' Routines for displaying which side (left or right)
'  is currently being triggered  
f_flash:					' Fast flash display LED
  for b0 = 1 to 4 
    high LED
    pause 30
    low LED
    pause 30
  next b0
  return

s_flash:					' Slow flash display LED
  for b0 = 1 to 4 
    high LED
    pause 75
    low LED    
    pause 75    
  next b0
  return