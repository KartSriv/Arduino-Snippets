main:
  high IR						 ' Toggle output high for 5 ms
  pause 5
  low IR
  pause 1
  
  pwmout IR, 25, 52  ' Approx.38.4 kHz for 50 ms
  pause 50              
  pwmout IR, 0, 0    ' Turn off PWM
  pause 20           ' 20 ms "quiet" time
  
  goto main					 ' loop
