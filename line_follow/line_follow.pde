/*
 ArdBot line following demo using
  2 reflective sensors
 Requires Arduino IDE version 0017 
  or later (0019 or later preferred)
*/

#include <Servo.h> 

Servo servoLeft;            // Define left servo
Servo servoRight;           // Define right servo
const int lineLSense = A3;
const int lineRSense = A4;

int irReflectR = 0;
int irReflectL = 0;
int thresh = 400;

void setup() { 
  servoLeft.attach(10);  // Left servo pin D10
  servoRight.attach(9);  // Right server pin D9
} 

void loop() { 

  // Read reflective sensors
  irReflectL = analogRead(lineLSense);  
  irReflectR = analogRead(lineRSense);

  if (irReflectL >= thresh && irReflectR >= thresh) {
    line_forward();   // on line
  }
  
  if (irReflectL >= thresh && irReflectR <= thresh) {
    line_spinLeft();  // veering off right
    delay(4);
  }
  
  if (irReflectL <= thresh && irReflectR >= thresh) {
    line_spinRight();  // veering off left
    delay(4);
  }

  // If line is lost try to reacquire
  if (irReflectL < thresh && irReflectR < thresh) {
    line_spinRight();
    delay(20);
  }
  
}

// Motion routines for line following
void line_forward() {
  servoLeft.write(0);
  servoRight.write(180);
}

void line_slipRight() {
  servoLeft.write(90);
  servoRight.write(180);
}
void line_slipLeft() {
  servoLeft.write(0);
  servoRight.write(90);
}
void line_spinRight() {
  servoLeft.write(180);
  servoRight.write(180);
}
void line_spinLeft() {
  servoLeft.write(0);
  servoRight.write(0);
}

