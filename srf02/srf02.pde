#include <Wire.h>

#define srfAddress 0x70                     // Address of the SRF02
#define cmdByte 0x00                        // Command byte
#define rangeByte 0x02                      // Byte for start of ranging data
byte byteHigh = 0x00;                       // Stores high byte from ranging
byte byteLow = 0x00;                        // Stored low byte from ranging

void setup(){
  Serial.begin(9600);
  Wire.begin();                               
  delay(200);                               // Wait briefly for powerup
}

void loop(){
  int rangeData = getRange();               // Get range (call getRange)
  Serial.print("Range= ");
  Serial.println(rangeData, DEC);           // Print result
  delay(100);                               // Wait before looping
}

int getRange(){                             // Ranging function
  
  int range = 0; 
  
  Wire.beginTransmission(srfAddress);       // Begin I2C communications
  Wire.send(cmdByte);                       // Send Command byte
  Wire.send(0x50);                          // Start ranging, return inches
  Wire.endTransmission();
  delay(100);                               // Wait for ranging to finish
  
  Wire.beginTransmission(srfAddress);
  Wire.send(rangeByte);                     // Get range data
  Wire.endTransmission();
  Wire.requestFrom(srfAddress, 2);          // Request 2 bytes from SRF module
  while(Wire.available() < 2);              // Wait for data to arrive
  byteHigh = Wire.receive();                // Get high byte
  byteLow = Wire.receive();                 // Get low byte
  range = (byteHigh << 8) + byteLow;        // Combine bytes into single integer
  return(range);                            // Return range value
}

