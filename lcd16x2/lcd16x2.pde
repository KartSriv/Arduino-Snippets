#include <LiquidCrystal.h>

// Initialize interface pins to LCD
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

void setup() {
  
  //Optional: Instead of using a 10 kohm pot,
  //  set the contrast of the LCD panel by connecting
  //  the Vo line (pin 3) of LCD to pin D9 of 
  //  the Arduino. Then set the contrast via PWM 
  //  to pin D9
  analogWrite(9, 10);
  
  // Configure numbers/rows for LCD
  lcd.begin(16, 2);

  // Print the message
  lcd.setCursor(0, 0);
  lcd.print("Robot Builder's");
  lcd.setCursor(0, 1);
  lcd.print("Bonanza, 4th Ed");
}

void loop() {
}

