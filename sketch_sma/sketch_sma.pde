int smaPin = 9;            // Connect SMA driver circuit to 
                           // pin D9
int ledPin = 13;           // Use integrated LED 

void setup()
{
  pinMode(smaPin, OUTPUT);      // Set pins as outputs
  pinMode(ledPin, OUTPUT); 
}

void loop()
{
  digitalWrite(smaPin, HIGH);   // Activate wire
  digitalWrite(ledPin, HIGH);   // Turn LED on  
  delay(250);                   // Keep on 1/4 second
  digitalWrite(smaPin, LOW);    // Deactivate wire
  digitalWrite(ledPin, LOW);    // Turn LED off
  delay(5000);                  // Relax/cool for 5 seconds  
  analogWrite(smaPin, 128);     // Activatde wire at 1/2 duty
  digitalWrite(ledPin, HIGH);   // Turn LED on    
  delay(750);                   // Keep on doer 1/2 second
  digitalWrite(smaPin, LOW);    // Deactivate wire  
  digitalWrite(ledPin, LOW);    // Turn LED off   
  delay(5000);                  // Relax/cool for 5 seconds   
}
