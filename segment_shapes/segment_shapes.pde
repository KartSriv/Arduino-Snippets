int latchPin = 8; 
int clockPin = 12; 
int dataPin = 11; 

void setup() {
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
}

void loop() {
  // Write a regular 7
  digitalWrite(latchPin, LOW);
  shiftOut(dataPin, clockPin, LSBFIRST, B11100000);
  digitalWrite(latchPin, HIGH);
  delay(1000);
  // Write some funky character
  digitalWrite(latchPin, LOW);
  shiftOut(dataPin, clockPin, LSBFIRST, B01110011);
  digitalWrite(latchPin, HIGH);
  delay(1000);
}