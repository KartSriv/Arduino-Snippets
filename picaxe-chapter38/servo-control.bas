' Servo control

init: 
  servo 4,150       ' Set up servo

main: 
  servopos 4,100  ' Move servo to one end
  pause 2500      ' Pause 2.5 seconds
  servopos 4,200  ' Move servo to other end
  pause 2500      ' Pause again 
  goto main       ' Repeat
