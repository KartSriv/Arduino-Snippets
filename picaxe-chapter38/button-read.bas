'Read a button

main:
  if pin1 = 1 then flash  ' 1 = switch closed
  low 4       ' Turn off Out4
  pause 10
  goto main   ' Repeat

flash:
  high 4      ' Turn off Out4
  pause 1000  ' Wait 1 second
  goto main
