const int alarmPin = 2;     
const int ledPin =  13;      

void setup() {
  pinMode(ledPin, OUTPUT);      
  pinMode(alarmPin, INPUT);     
}

void loop(){
  if (digitalRead(alarmPin) == HIGH) {     
    digitalWrite(ledPin, HIGH);  
  } 
  else {
    digitalWrite(ledPin, LOW); 
  }
}
