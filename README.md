
<h1 align="center">
  <br>
  <a href="https://karthiksrivijay.me/ArduinoSnippets.html"><img src="https://www.arduino.cc/en/uploads/Trademark/ArduinoCommunityLogo.png" alt="Markdownify" width="200"></a>
</h1>

<h4 align="center">Random Arduino Codes I think are useful. To download all files <a href="https://codeload.github.com/KartSriv/Arduino-Snippets/zip/master" target="_blank">Click Here</a>.</h4>
<h4 align="center">For every 100 downloads I'll update the repository. </h4>

  <a href="https://saythanks.io/to/KartSriv"><img src="https://img.shields.io/badge/SayThanks.io-%E2%98%BC-1EAEDB.svg"> <a href="https://app.codacy.com/app/KartSriv/Arduino-Snippets?utm_source=github.com&utm_medium=referral&utm_content=KartSriv/Arduino-Snippets&utm_campaign=Badge_Grade_Dashboard"> <img src="https://api.codacy.com/project/badge/Grade/c0bb070e91b04fe1a87fc4ad9bfff2b7"> <a> <img src="https://img.shields.io/github/license/KartSriv/Arduino-Snippets.svg"><a> <img src="https://img.shields.io/github/downloads/KartSriv/Arduino-Snippets/total.svg"></a>


**NOTE:** I just got promoted to 11th grade which means I have to stay away from my computer. I can't really keep on updating the repo anymore. If I don't respond to some of your PRs then I am sorry I was too busy. I think I can make some time later but let's see.

**Added 31 Working and Tested Snippets with courtesy to Google Code Archive** - 8/6/2017 <br>
**Added 41 Working and Tested Snippets with courtesy to Robotoid** - 7/15/2018 <br>
**Added 16 Working and Tested Snippets with courtesy to Sparkfun** - 2/10/2019 <br>
**Added 38 Working and Tested Snippets with courtesy to Robotics Universe** - 3/10/2019 <br>
**Added 60 Working and Tested Snippets with courtesy to Blynk IOT** - 10/8/2019 <br>

[![ForTheBadge built-with-science](http://ForTheBadge.com/images/badges/built-with-science.svg)](https://github.com/KartSriv/)
[![ForTheBadge built-with-love](http://ForTheBadge.com/images/badges/built-with-love.svg)](https://github.com/KartSriv/)<br>
<br> 
<h1 align="center"><b>List of Programs included</b></h1>
<p align="center"><b>ADXL3xx		<br> 
ASCIITable		<br> 
AnalogInOutSerial		<br> 
AnalogInput		<br> 
AnalogReadSerial		<br> 
AnalogWriteMega		<br> 
ArduinoISP		<br> 
Arrays		<br> 
BareMinimum		<br> 
Blink		<br> 
BlinkWithoutDelay	<br> 
Blynk	<br> 
Button		<br> 
Calibration		<br> 
CharacterAnalysis		<br> 
Debounce		<br> 
DigitalInputPullup		<br> 
DigitalReadSerial		<br> 
Dimmer		<br> 
Fade		<br> 
Fading		<br> 
ForLoopIteration	<br> 
Graph		<br> 
IfStatementConditional<br> 	
eyboard		<br> 
KeyboardAndMouseControl		<br> 
Keypad(4X4)		<br> 
Knock		<br> 
Memsic2125		<br> 
Midi		<br> 
Mouse		<br> 
MultiSerial		<br>  
PhysicalPixel		<br> 
Ping		<br> 
ReadASCIIString		<br> 
ReadAnalogVoltage		<br> 
Relay		<br> 
RowColumnScanning	<br> 
IK_Circuit_1A-Blink		<br> 
SIK_Circuit_1B-Potentiometer		<br> 
SIK_Circuit_1C-Photoresistor		<br> 
SIK_Circuit_1D-RGBNightlight		<br> 
SIK_Circuit_2A-Buzzer		<br> 
SIK_Circuit_2B-DigitalTrumpet		<br> 
SIK_Circuit_2C-SimonSays		<br> 
SIK_Circuit_3A-Servo		<br> 
SIK_Circuit_3B-DistanceSensor		<br> 
SIK_Circuit_3C-MotionAlarm		<br> 
SIK_Circuit_4A-LCDHelloWorld		<br> 
SIK_Circuit_4B-TemperatureSensor		<br> 
SIK_Circuit_4C-DIYWhoAmI		<br> 
SIK_Circuit_5A-MotorBasics		<br> 
SIK_Circuit_5B-RemoteControlRobot		<br> 
SIK_Circuit_5C-AutonomousRobot		<br> 
SerialCallResponse		<br> 
SerialCallResponseASCII		<br> 
SerialEvent		<br> 
SerialPassthrough		<br> 
Smoothing		<br> 
StateChangeDetection		<br> 
StringAdditionOperator		<br> 
StringAppendOperator		<br> 
StringCaseChanges		<br> 
StringCharacters		<br> 
StringComparisonOperators		<br> 
StringConstructors		<br> 
StringIndexOf		<br> 
StringLength		<br> 
StringLengthTrim		<br> 
StringReplace		<br> 
StringStartsWithEndsWith	<br> 
tringSubstring		<br> 
StringToInt		<br> 
UltrasonicWithLED		<br> 
Ultrasonic_Tracker		<br> 
VirtualColorMixer		<br> 
WhileStatementConditional		<br> 
accel2		<br> 
arduino_motor_control<br> 	
rduino_servo		<br> 
arduino_test		<br> 
barGraph		<br> 
basic_servo_test		<br> 
bs2-chapter39		<br> 
button_press		<br> 
button_press_enhanced		<br> 
cmps09		<br> 
flyeye		<br> 
flyeye_thresh		<br> 
gp2d12		<br> 
hex3bot		<br> 
ir-bot		<br> 
irpd-picaxe		<br> 
irpd		<br> 
joystick		<br> 
lcd16x2		<br> 
led		<br> 
line_follow		<br> 
multi_led		<br> 
p02_SpaceshipInterface		<br> 
p03_LoveOMeter		<br> 
p04_ColorMixingLamp		<br> 
p05_ServoMoodIndicator		<br> 
p06_LightTheremin		<br> 
p07_Keyboard		<br> 
p08_DigitalHourglass		<br> 
p09_MotorizedPinwheel		<br> 
p10_Zoetrope		<br> 
p11_CrystalBall		<br> 
p12_KnockLock		<br> 
p13_TouchSensorLamp		<br> 
p14_TweakTheArduinoLogo		<br> 
p15_HackingButtons		<br> 
picaxe-chapter38		<br> 
propane		<br> 
rainbow_led		<br> 
segment		<br> 
segment_shapes		<br> 
sketch_sma		<br> 
smoke_detector		<br> 
sonyremote/sonyremote		<br> 
speakjet		<br> 
srf02		<br> 
srf05		<br> 
switchCase		<br> 
switchCase2		<br> 
switch_mux		<br> 
switch_piso		<br> 
temperature		<br> 
toneKeyboard		<br> 
toneMelody		<br> 
toneMultiple		<br> 
tonePitchFollower		<br> 
tones		<br> 
tricolor		<br> 
two-servos<br> </b></P>
