const int eyeRight = A0;
const int eyeTop = A1;
const int eyeBottom = A2;
const int eyeLeft = A3;
const int irTrigger = 8;
const int ledRight = 9;
const int ledTop = 10;
const int ledBottom = 11;
const int ledLeft = 12;

int thresh = 250;

void setup() { 

  pinMode(irTrigger, OUTPUT);
  pinMode(ledRight, OUTPUT);
  pinMode(ledTop, OUTPUT);
  pinMode(ledBottom, OUTPUT);
  pinMode(ledLeft, OUTPUT);  
  digitalWrite(irTrigger, HIGH);  
  
} 
 
void loop() { 
  
  if(analogRead(eyeTop)>thresh)
    digitalWrite(ledTop, HIGH);
  else
   digitalWrite(ledTop, LOW);
   
  if(analogRead(eyeLeft)>thresh)
    digitalWrite(ledLeft, HIGH);
  else
   digitalWrite(ledLeft, LOW);

  if(analogRead(eyeRight)>thresh)
    digitalWrite(ledRight, HIGH);
  else
   digitalWrite(ledRight, LOW);

  if(analogRead(eyeBottom)>thresh)
    digitalWrite(ledBottom, HIGH);
  else
   digitalWrite(ledBottom, LOW);

}
