//Libraries
#include "Ultrasonic.h"

//Define pins ultrasonic(trig,echo)
Ultrasonic ultrasonic(A0, A1);

//Variables
int distance;
const int l = 13;
void setup() {
  Serial.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop()
{
  distance = ultrasonic.Ranging(CM); //Use 'CM' for centimeters or 'INC' for inches
  if (distance>15) {
  Serial.print("Test Status: FAILED ");
  //Print distance...
  Serial.print("ERROR 1: Arduino not set properly. ");
  distance=distance-distance;
  Serial.print(distance);
   }
  else if (distance==7) {
  Serial.print("Test Status: PASSED ");
  //Print distance...
  Serial.print("No cars on the road. : ");
  delay(1000);
  }
  else if (distance<7) {
    digitalWrite(l, HIGH);
    Serial.print("A car found on the road. ");
    //Print distance...
    Serial.print("Car found at: ");
    Serial.print(distance);
    Serial.println("cm");
    //every 1sec.
    delay(1000);
  }

}


